# CS 445
Code repo for PSU CS445 (Machine Learning).

While this repository is now public, I kindly ask that any students at PSU that happen upon this repository to not copy any of its contents. Academic Misconduct is
taken very seriously at PSU, and can get you (and me) into serious trouble.
Please visit https://www.pdx.edu/dos/academic-misconduct for more information.

This code is licenced under the MIT License. See LICENSE file in the root of
the git repo for more infromation.