from __future__ import division
from scipy.special import expit
from sklearn.metrics import confusion_matrix
from tqdm import tqdm
import numpy as np
import mnist
import csv

#"constants", aka values that shouldn't be changing like the raw training data
bias = 1
print("Downloading mnist data...")
training_images = mnist.train_images()
training_labels = mnist.train_labels()
testing_images = mnist.test_images()
testing_labels = mnist.test_labels()

#normalize
training_images = np.array(training_images, dtype=float)
testing_images = np.array(testing_images, dtype=float)

training_images = training_images/255
testing_images = testing_images/255

#get # of hidden neurons from user
hidden_neurons = input("Enter number of neurons in hidden layer: ")
learn_rate = 0.1
momentum = 0.9

#WEIGHT, OUTPUT, ERROR, AND DELTA_WEIGHT MATRICES/VECTORS

#input to hidden
hiddenWeights = np.random.uniform(-0.05, 0.05, (hidden_neurons, 785))
hiddenDeltaW = np.zeros((hidden_neurons, 785), dtype=float)
hiddenOutputs = np.zeros((hidden_neurons+1, 1), dtype=float)
hiddenErrors = np.zeros((hidden_neurons,1), dtype=float)

#hidden to output
outputWeights = np.random.uniform(-0.05, 0.05, (10, hidden_neurons+1))
outDeltaW = np.zeros((10, hidden_neurons+1), dtype=float)
outputOutputs = np.zeros((10,1), dtype=float)
outputErrors = np.zeros((10,1), dtype=float)

#activation function, leverages scipy's built in logistic function
def act(x):
    return expit(x)
#vectorized version of the activation function so i can apply it across vectors
sigmoid = np.vectorize(act)

def forwardProp(image):
    global hiddenOutputs
    global outputOutputs

    #flatten the image to a 1x785 vector
    flat = image.flatten()
    flat = np.insert(flat, 0, bias)

    hiddenOutputs = sigmoid(np.dot(hiddenWeights, flat))
    hiddenOutputs = np.insert(hiddenOutputs, 0, bias)
    outputOutputs = sigmoid(np.dot(outputWeights, hiddenOutputs))

    out = np.full(10, 0.1)
    out[np.argmax(outputOutputs)] = 0.9
    return out

#there are a lot of weird reshapes in this function, python insisted on changing the shapes of
#the numpy arrays seemingly at random
def backprop(image, label):
    global outputOutputs
    global hiddenOutputs
    global outputErrors
    global hiddenErrors
    global outputWeights
    global hiddenWeights
    global outDeltaW
    global hiddenDeltaW

    labelV = np.full(10, 0.1)
    labelV[label] = 0.9
    outputErrors = outputOutputs * (1-outputOutputs) * (labelV-outputOutputs)
    hiddenErrors = hiddenOutputs*(1-hiddenOutputs) * np.dot(np.transpose(outputWeights), outputErrors)

    #flatten the image to a 785x1 vector
    flat = image.flatten()
    flat = np.insert(flat, 0, bias)
    flat = flat.reshape(785,1)
    outputErrors = outputErrors.reshape(10,1)

    #weight updates
    outWeightsD = (learn_rate * np.dot(outputErrors.reshape((len(outputErrors), 1)), hiddenOutputs.reshape(1, len(hiddenOutputs)))) + (momentum * outDeltaW)
    outputWeights += outWeightsD

    hiddenErrors = hiddenErrors.reshape(hidden_neurons+1,1)
    hidWeightsD = (learn_rate * np.dot(hiddenErrors[1:], np.transpose(flat))) + momentum * hiddenDeltaW
    hiddenWeights += hidWeightsD

    outDeltaW = outWeightsD
    hiddenDeltaW = hidWeightsD

#training driver function, iterates over the training data, forward and back props over the network
def train():
    print("Training...")
    randomized = np.arange(0, training_images.shape[0])
    np.random.shuffle(randomized)
    for i in tqdm(range(training_images.shape[0])):
        forwardProp(training_images[randomized[i]])
        backprop(training_images[randomized[i]], training_labels[randomized[i]])

#testing driver function, iterates over a given data set, forwardpropping for guesses, and returns the accuracy of the model
def test(images, labels):
    right = 0
    randomized = np.arange(0, images.shape[0])
    np.random.shuffle(randomized)

    for i in tqdm(range(images.shape[0])):
        out = forwardProp(images[randomized[i]])

        if np.argmax(out) == labels[randomized[i]]:
            right += 1

    return right/float(images.shape[0]) * 100

#mitigates sklearn to create a confusion matrix for us
def get_confusion_matrix(images, labels):
    guesses = np.arange(images.shape[0])
    outLabels = np.zeros(images.shape[0], dtype=int)
    for i in range(images.shape[0]):
        #this line was the culprit for the non-working confusion matrix, it was just forwardprop with no arguments
        guess = forwardProp(images[i])
        guesses[i] = np.argmax(guess)
        outLabels[i] = labels[i]

    return confusion_matrix(outLabels, guesses)

epochs = 50
trainAccs = []
testAccs = []
for i in range(0, epochs):
    print("Epoch " + str(i))
    if i != 0:
        train()

    trAcc = test(training_images, training_labels)
    teAcc = test(testing_images, testing_labels)
    trainAccs.append(trAcc)
    testAccs.append(teAcc)
    print("Accuracy (Training): " + str(trAcc))
    print("Accuracy (Testing): " + str(teAcc))
    print("---------------------------------------------")

out = open(str(hidden_neurons)+'neuronsData.csv', 'w')
with out:
    writer = csv.writer(out)
    writer.writerow(trainAccs)
    writer.writerow(testAccs)

confusion = get_confusion_matrix(testing_images, testing_labels)
cout = open(str(hidden_neurons) + 'Confusion.csv', 'w')
with cout:
    writer = csv.writer(cout)
    writer.writerows(confusion)

print("Done.")
