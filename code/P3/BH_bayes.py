from __future__ import division
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import numpy as np
import pandas
import csv

#supress inevitable overflow warnings, numpy will approximate and that's good enough for me
np.seterr(all='ignore')

def gaussDist(x, sdev, mean):
    const = 1/(np.sqrt(2*np.pi)*sdev)
    expo = np.exp(((x-mean)**2)/(2*(sdev**2)))
    return const * expo

#vectorize gaussDist so I can apply it across vectors later down the line
gnb = np.vectorize(gaussDist)

print("Parsing data...")
#since it wasn't specified we had to write our own code to do this, lets use pandas and sklearn to
#parse the data and make our training sets
data = pandas.read_csv('./spambase/spambase.data', header = None)
data.rename(columns={57:'is_spam'}, inplace=True)

print("Generating test/train sets...")
spam = data[data['is_spam'] == 1]
notspam = data[data['is_spam'] == 0]
train_spam, test_spam = train_test_split(spam, train_size=0.60, test_size=0.40)
train_not, test_not = train_test_split(notspam, train_size=0.60, test_size=0.40)

#convert our teaining and test sets to numpy arrays, makes it easier to work with
#and do math on
train_spam = train_spam.to_numpy()
test_spam = test_spam.to_numpy()
train_not = train_not.to_numpy()
test_not = test_not.to_numpy()

#generate probabalistic model
prob_spam = train_spam.shape[0]/float(train_spam.shape[0]+train_not.shape[0])

prob_not = train_not.shape[0]/float(train_not.shape[0]+train_spam.shape[0])

print("Training Probabilities:")
print("P(Spam) = " + str(prob_spam))
print("P(Not) = " + str(prob_not))

print("Generating probabalistic model...")
#merge the training set together
#calculate mean & standard dev for every feature
meansSpam = np.zeros(57, dtype=float)
sdevsSpam = np.zeros(57, dtype=float)

meansNot = np.zeros(57, dtype=float)
sdevsNot = np.zeros(57, dtype=float)

for i in range(57):
    colSpam = train_spam[:, [i]]
    colNot = train_not[:, [i]]
    meansSpam[i] = np.mean(colSpam)
    meansNot[i] = np.mean(colNot)
    sdev = np.std(colSpam)
    sdevNot = np.std(colNot)

    if sdev == 0:
        sdev = 0.0001
    if sdevNot == 0:
        sdevNot = 0.0001

    sdevsSpam[i] = sdev
    sdevsNot[i] = sdevNot

#perform naive bayesian learning
print("Performing Naive Bayesian Learning...")
#make arrays for confusion matrix
guesses = np.zeros((test_spam.shape[0] + test_not.shape[0]))
testLabels = np.concatenate((test_spam, test_not))
testLabels = testLabels[:, [57]]

mergedTest = np.concatenate((test_spam, test_not))
correct = 0
for i in tqdm(range(mergedTest.shape[0])):
    row = mergedTest[i]
    row = row[:-1]

    spamConf = gnb(row, sdevsSpam, meansSpam)
    notConf = gnb(row, sdevsNot, meansNot)

    spamPred = np.log(np.prod(spamConf))
    notPred = np.log(np.prod(notConf))
    predV = np.array([spamPred, notPred])
    pred = np.argmax(predV)
    guesses[i] = pred

    if pred == testLabels[i]:
        correct += 1

accuracy = correct/testLabels.shape[0]
conf = confusion_matrix(testLabels, guesses)
print(conf)
print("Accuracy: " + str(accuracy * 100))
