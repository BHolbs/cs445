from __future__ import division
from sklearn.metrics import confusion_matrix
from tqdm import tqdm
import numpy as np
import mnist
import csv

# begin class definition for perceptron
class Perceptron:
    def __init__(self, learn):
        self.weights = np.random.uniform(-0.05, 0.05, 785)
        self.bias = 1

        self.learn_rate = learn

    def predict(self, inImage):
        flattened = inImage.flatten()
        flattened = np.insert(flattened, 0, self.bias)
        return np.dot(flattened, self.weights)

    def act(self, dot):
        if dot > 0:
            return 1
        else:
            return 0

    # updates the weights inside of the perceptron
    def update_weight(self, diff, image):
        self.weights += (self.learn_rate * diff * image)
#end class definition

#start helper function definitions

# wrapper function for updating the weight for each perceptron
#
# takes the array of perceptrons as an argument, the matrix of pixels (which we flatten to a 1-d array for the dot product
# the actual index of the correct answer for the image we're on, and the dot product we got in learn)
def update_weights(pArray, image, actualIndex, dot):
    flattened = image.flatten()
    flattened = np.insert(flattened, 0, 1)
    for i in range(0, 10):
        t = 0
        #if this index is where the right guess is
        if i == actualIndex:
            t = 1
        #check if the dot product is > 0
        y = pArray[i].act(dot)
        diff = t-y

        pArray[i].update_weight(diff, flattened)

# the main workhorse, takes an array of perceptrons as an argument
def learn(pArray):
    print("Learning...")

    # create an array of the indexes for the training images that will also be the same as
    # the indexes for the corresponding label -
    # training_labels[n] is the label for training_images[n]
    # then randomize the array so we aren't just memorizing, but learning
    randomizedIndexes = np.arange(training_images.shape[0])
    np.random.shuffle(randomizedIndexes)

    #for every image
    for i in tqdm(range(0, training_images.shape[0])):
        guesses = np.zeros(10, dtype=float)
        #choose a random image
        randomImage = randomizedIndexes[i]

        #for every perceptron
        for j in range(0, 10):
            guesses[j] = pArray[j].predict(training_images[randomImage])

        # find the max value, that's our prediction
        prediction = np.argmax(guesses)

        # if the network's prediction does not match the actual label
        # update all of the perceptrons' weights
        if prediction != training_labels[randomImage]:
            update_weights(pArray, training_images[randomImage], training_labels[randomImage], prediction)


# does very nearly the same thing as learn, but it doesn't update the weights if the guess was wrong
def test(pArray, imagesToTest, labels):
    correct = 0
    randomizedIndexes = np.arange(imagesToTest.shape[0])
    np.random.shuffle(randomizedIndexes)

    #for every image in the data set
    for i in tqdm(range(0, imagesToTest.shape[0])):
        guesses = np.zeros(10, dtype=float)
        randomImage = randomizedIndexes[i]

        #for every perceptron make a guess
        for j in range(0, 10):
            guesses[j] = pArray[j].predict(imagesToTest[randomImage])

        # the highest index is our guess, which is conveniently the same as the actual # we guessed
        prediction = np.argmax(guesses)

        # if our prediction matches the label
        if prediction == labels[randomImage]:
            correct += 1

    return (correct / float(imagesToTest.shape[0])) * 100

def get_confusion_matrix(pArr, images, labels):
    allGuesses = np.arange(images.shape[0])
    for i in range(0, images.shape[0]):
        guesses = np.zeros(10, dtype=float)

        for j in range(0, 10):
            guesses[j] = pArr[j].predict(images[i])

        prediction = np.argmax(guesses)

        allGuesses[i] = prediction

    return confusion_matrix(labels, allGuesses)


#end helper function definitions

#download and normalize mnist data
print("Downloading mnist data...")
training_images = mnist.train_images()
training_labels = mnist.train_labels()
testing_images = mnist.test_images()
testing_labels = mnist.test_labels()

#force elements to be floats and then normalize the data
training_images = np.array(training_images, dtype=float)
testing_images = np.array(testing_images, dtype=float)

training_images = training_images/255
testing_images = testing_images/255

#get learning rate from user
learningRate = input("Enter learning rate (0.1, 0.01, or 0.001): ")

while learningRate != 0.1 and learningRate != 0.01 and learningRate != 0.001:
    learningRate = input("Enter learning rate (0.1, 0.01, or 0.001): ")

print("Starting with learning rate " + str(learningRate) + "...")

#initialize array of perceptrons
p1arr = np.zeros(10, dtype=object)
#doing it in a for loop, if i do it any other way i observed all the weights being the same
for p in range(0, 10):
    p1arr[p] = Perceptron(learningRate)

#get to learning
epochs = 5
lastAcc = test(p1arr, training_images, training_labels)
trainingAccuracies = [lastAcc]
firstTestAcc = test(p1arr, testing_images, testing_labels)
testingAccuracies = [firstTestAcc]
print("Epoch 0")
print("Accuracy (Training): " + str(lastAcc))
print("Accuracy (Testing): " + str(firstTestAcc))
print("------------------------------------------------------------")
for i in range(1, epochs):
    print("Epoch " + str(i))
    learn(p1arr)
    currAcc = test(p1arr, training_images, training_labels)
    testAcc = test(p1arr, testing_images, testing_labels)
    print("Accuracy (Training): " + str(currAcc))
    print("Accuracy (Testing): " + str(testAcc))

    trainingAccuracies.append(currAcc)
    testingAccuracies.append(testAcc)
    print("------------------------------------------------------------")

    if abs(lastAcc - currAcc) < 0.01:
        print("Learning accuracy has converged, stopping now.")
        break

    lastAcc = currAcc

#write data points for graphs, matplotlib was not cooperating with your python install
out = open(str(learningRate) + 'data.csv', 'w')
with out:
    writer = csv.writer(out)
    writer.writerow(trainingAccuracies)
    writer.writerow(testingAccuracies)

#generate confusion matrix after training
confusion_matrix = get_confusion_matrix(p1arr, testing_images, testing_labels)
cout = open(str(learningRate) + 'confMatrix.csv', 'w')
with cout:
    writer = csv.writer(cout)
    writer.writerows(confusion_matrix)

print("Done.")
