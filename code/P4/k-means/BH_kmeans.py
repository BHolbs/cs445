from __future__ import division
from tqdm import tqdm
from scipy.spatial import distance
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy as cp

def findClosest(data, centroids, n):
    for i in range(data.shape[0]):
        dist = np.zeros(n)
        for j in range(n):
            currPoint = data[i]
            point = currPoint[:-1]
            point = point.flatten()
            dist[j] = distance.euclidean(point, centroids[j])
        data[i][2] = np.argmin(dist)

#get average of cluster for a given centroid
def getAvg(data, centroid, i):
    xSum = 0
    ySum = 0
    count= 0
    for j in range(data.shape[0]):
        if(data[j][2] == i):
            count += 1
            xSum += data[j][0]
            ySum += data[j][1]

    if count != 0:
        xAvg = xSum/count
        yAvg = ySum/count
        return np.array([xAvg, yAvg])
    else:
        return centroid


print("Loading data...")
data = pd.read_fwf('../GMM_data_fall2019.txt', header=None)

#3rd column is a notion of which centroid it's closest to
#we assume they belong to some nebulous 0th cluster at the beginning
data['2'] = 0
data = data.to_numpy();

k = input("Enter number of clusters desired: ")
print("Beginning k-means...")
#grab bounds for our guesses, not very useful to guess outside of where the data is
xMax = np.amax(data[:, 0])
xMin = np.amin(data[:, 0])

yMax = np.amax(data[:, 1])
yMin = np.amin(data[:, 1])

centroids = np.zeros((k,2))
centroids[:, 0] = np.random.uniform(xMin, xMax, (k))
centroids[:, 1] = np.random.uniform(yMin, yMax, (k))

avgChange = 100
iterations = 0
while avgChange > 0.00001:
    if avgChange != 100:
        plt.clf()
    plt.scatter(data[:, 0], data[:, 1], c = data[:, 2])
    plt.scatter(centroids[:, 0], centroids[:, 1], c='k', marker='+');
    plt.show(block=False)
    plt.savefig(str(k) + "clusters/" + 'iter' + str(iterations) + '.png')
    plt.pause(0.01)

    findClosest(data, centroids, k)

    changes = 0.0
    for j in range(k):
        oldVal = cp.deepcopy(centroids[j])
        centroids[j] = getAvg(data, centroids[j], j)
        changes += distance.euclidean(oldVal, centroids[j])


    avgChange = changes/float(k)
    iterations += 1

#calculate the sum of squares error
total = 0.0
distanceSums = np.zeros(k)
for i in range(k):
    innerSum = 0.0
    for j in range(data.shape[0]):
        if data[j][2] == i:
            point = data[j]
            point = point[: -1]
            point = point.flatten()
            innerSum += distance.euclidean(centroids[i], point)
    total += innerSum

print('Sum of squares error: ' + str(total))


print('Converged in ' + str(iterations) + ' iterations')
