from __future__ import division
from tqdm import tqdm
from scipy.spatial import distance
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy as cp

m = 2

def getCentroids(data, weights, c):
    global m
    centroids = np.zeros((c,2))
    for i in range(c):
        xNumerator, yNumerator, denom = 0.0, 0.0, 0.0
        for j in range(data.shape[0]):
            xNumerator += data[j][0] * weights[j][i]**m
            yNumerator += data[j][1] * weights[j][i]**m
            denom += weights[j][i]**2
        centroids[i][0] = xNumerator/denom
        centroids[i][1] = yNumerator/denom

    return centroids


def updateWeights(data, weights, centroids, c):
    global m
    #get all the distances
    distances = np.zeros((data.shape[0], c))
    for ci in range(centroids.shape[0]):
        for d in range(data.shape[0]):
            distances[d][ci] = distance.euclidean(centroids[ci], data[d])

    distances[distances == 0] = 1e-6

    newWeights = np.zeros((data.shape[0],c))
    for i in range(c):
        for j in range(data.shape[0]):
            newWeights[j][i] = ((1/distances[j][i])**(1/m-1))/(np.sum((1/distances[j,:])**(1/m-1)))

    return newWeights


def updateColor(weights, c):
    colors = np.zeros(data.shape[0])
    for i in range(weights.shape[0]):
        colors[i] = np.argmax(weights[i])
    return colors


print("Loading data...")
data = pd.read_fwf('../GMM_data_fall2019.txt', header=None)
data = data.to_numpy()
colors = np.zeros(data.shape[0])

c = input("Enter number of clusters desired: ")
print("Beginning fuzzy c-means...")

xMax = np.amax(data[:, 0])
xMin = np.amin(data[:, 0])

yMax = np.amax(data[:, 1])
yMin = np.amin(data[:, 1])

weights = np.random.uniform(0, 1, (data.shape[0], c))
avgChange = 1
centroids = np.zeros((c,2))
centroids[:, 0] = np.random.uniform(xMin, xMax, (c))
centroids[:, 1] = np.random.uniform(yMin, yMax, (c))
iterations = 0
while avgChange > 1e-20:
    if avgChange != 1:
        plt.clf()
    plt.scatter(data[:,0], data[:, 1], c = colors)
    plt.scatter(centroids[:, 0], centroids[:,1], c='k', marker='+')
    plt.show(block=False)
    plt.savefig(str(c) + "clusters/" + 'iter' + str(iterations) + '.png')
    plt.pause(0.25)

    #calculate stuff for the next interval
    oldWeights = cp.deepcopy(weights)
    weights = updateWeights(data, weights, centroids, c)
    centroids = getCentroids(data, weights, c)
    colors = updateColor(weights, c)

    avgChange = abs(np.average(oldWeights-weights))
